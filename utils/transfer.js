var gbk_encode = require('gbk.js');

function hexStringToBuff(str) {
  const buffer = new ArrayBuffer((this.sumStrLength(str)) + 1);
  const dataView = new DataView(buffer);
  var data = str.toString();
  var p = 0;
  for(var i=0;i<data.length;i++){
    if (this.isCN(data[i])){ //是中文
      //调用GBK 转码
      var t = gbk_encode.$URL.encode(data[i]);
      for (var j = 0; j < 2; j++) {
        var temp = parseInt(t[j * 2] + t[j * 2 + 1], 16)
        dataView.setUint8(p++, temp)
      }
    }else{ 
      var temp = parseInt(data.charCodeAt(i).toString(16), 16)
      dataView.setUint8(p++,temp)
    }
  }
  return buffer;
}


//计算arraybuffer的长度
function sumStrLength(str){
var length=0;
var data = str.toString();
for (var i = 0; i < data.length; i++) {
  if (this.isCN(data[i])) { //是中文
    length += 2;
  } else {
    length += 1;
  }
}
return length;
}

//js正则验证中文
function isCN(str){
if (/[^\x00-\xFF]+$/.test(str)) {
  return true;
} else {
  return false;
}
}

//汉字转码
function hexStringToArrayBuffer (str){
const buffer = new ArrayBuffer((str.length / 2) + 1)
const dataView = new DataView(buffer)
for (var i = 0; i < str.length / 2; i++) {
  var temp = parseInt(str[i * 2] + str[i * 2 + 1], 16)
  dataView.setUint8(i, temp)
}
dataView.setUint8((str.length / 2), 0x0a)
return buffer;
}

//返回八位数组
function subString(str) {
var arr = [];
if (str.length > 8) { //大于8
  for (var i = 0; (i * 8) < str.length; i++) {
    var temp = str.substring(i * 8, 8 * i + 8);
    arr.push(temp)
  }
  return arr;
}else{
  return str
}
}

//不带有汉字
function hexStringToArrayBufferstr(str){
    let val = ""
  for (let i = 0; i < str.length; i++) {
    if (val === '') {
      val = str.charCodeAt(i).toString(16)
    } else {
      val += ',' + str.charCodeAt(i).toString(16)
    }
  }
  val += "," + "0x0a";
  console.log(val)
  // 将16进制转化为ArrayBuffer
  return new Uint8Array(val.match(/[\da-f]{2}/gi).map(function (h) {
    return parseInt(h, 16)
  })).buffer
}

module.exports={
  hexStringToBuff: hexStringToBuff,
  sumStrLength: sumStrLength,
  isCN: isCN,
  hexStringToArrayBuffer: hexStringToArrayBuffer,
  subString: subString,
  hexStringToArrayBufferstr: hexStringToArrayBufferstr
}