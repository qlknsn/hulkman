// pages/user/user.js
import {
  subscriberMine
} from "../../request/getIndexData"
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userinfo:{}
  },
  getUser(){
    subscriberMine().then(res=>{
      if(res.data.error&&res.data.error.code == 401){
        wx.showToast({
          title: res.data.error.message,
          icon:'none'
        })
        setTimeout(()=>{
          wx.reLaunch({
            url: '/pages/register/register',
          })
        },1000)
      }else{
        this.setData({
          userinfo:res.data.result
        })
      }
       
    })
  },
  feedback(){
    wx.showModal({
      title: '提示',
      content: "暂无此功能",
      showCancel: false
    })
  },
  aboutUs(){
    wx.showModal({
      title: '提示',
      content: "暂无此功能",
      showCancel: false
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

    // this.getUser()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

    this.getUser()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})