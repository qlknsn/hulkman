// pages/carownerinfo/carownerinfo.js
import {
  searchDistrict,
  subscriberMember
} from "../../request/getIndexData"
Page({

  /**
   * 页面的初始数据
   */
  data: {
    carInfo: {
      name: '',
      idCard: '',
      gender: 'MALE',
      legalAddress: '',
      phone: '',
      mailAddress: '',
      emergencyContacts: [{
        person: '',
        mobile: ''
      }],
    },
    zhenarray: [],
    zhenindex: 0,
  },
  getsearchDistrict() {
    let par = {
      parents: [
        "urn:bh:district:1",
      ],
      start:0,
      count:50
    }
    searchDistrict(par).then(res => {
      res.data.results.unshift({
        name: "-请选择-"
      })
      this.setData({
        zhenarray: res.data.results
      })
    })
  },
  bindPickerChangezhen: function (e) {
    console.log('picker发送选择改变，携带值为', e)
    let index = e.detail.value
    this.setData({
      zhenindex: index
    })
    this.data.carInfo.districtUrn = this.data.zhenarray[index].urn
  },
  userName(e) {
    console.log(e)
    this.data.carInfo.name = e.detail.value
  },
  userIcCard(e) {
    // if()
    this.data.carInfo.idCard = e.detail.value
  },
  unblurIdcard(e) {
    if (this.data.carInfo.idCard.length == 18) {
      this.data.carInfo.idCard;
    } else {
      wx.showToast({
        title: '身份证号为18位',
        icon: 'none',
        duration: 2000
      })
    }
  },
  userSex(e) {
    console.log(e)
    this.data.carInfo.gender = e.detail.value
  },
  userLegalAddress(e) {
    this.data.carInfo.legalAddress = e.detail.value
  },
  userPhone(e) {
    this.data.carInfo.phone = e.detail.value
  },
  unblurPhone(e) {
    if (this.data.carInfo.phone.length == 11) {
      this.data.carInfo.phone;
    } else {
      wx.showToast({
        title: '手机号为11位',
        icon: 'none',
        duration: 2000
      })
    }
  },
  userMailAddress(e) {
    this.data.carInfo.mailAddress = e.detail.value
  },
  jinjisuer(e) {
    this.data.carInfo.emergencyContacts[0].person = e.detail.value
  },
  jinjisuerphone(e) {
    this.data.carInfo.emergencyContacts[0].mobile = e.detail.value
  },
  submituserInfo() {
    console.log(this.data.carInfo)
    if (!this.data.carInfo.districtUrn) {
      wx.showToast({
        title: '请选择区域',
        icon: 'none',
        duration: 2000
      })
      return false;
    }
    if (!this.data.carInfo.idCard) {
      wx.showToast({
        title: '请填写身份证号',
        icon: 'none',
        duration: 2000
      })
      return false;
    }
    if (this.data.carInfo.idCard.length == 18) {
      this.data.carInfo.idCard;
    } else {
      wx.showToast({
        title: '身份证号为18位',
        icon: 'none',
        duration: 2000
      })
      return false;
    }
    if (!this.data.carInfo.phone) {
      wx.showToast({
        title: '请填写手机号',
        icon: 'none',
        duration: 2000
      })
      return false;
    }
    if (this.data.carInfo.phone.length == 11) {
      this.data.carInfo.phone;
    } else {
      wx.showToast({
        title: '手机号为11位',
        icon: 'none',
        duration: 2000
      })
      return false;
    }
    if (!this.data.carInfo.name) {
      wx.showToast({
        title: '请填写车主姓名',
        icon: 'none',
        duration: 2000
      })
      return false;
    }

    this.data.carInfo.districtUrn = wx.getStorageSync('districtId')

    subscriberMember(this.data.carInfo).then(res => {
      if (res.data.error) {
        console.info(res.data.error)
      } else {
        wx.setStorageSync('userUrn', res.data.result.urn)
        wx.setStorageSync('ownerName', res.data.result.name)
        wx.navigateTo({
          url: '/pages/insatllCarInfo/insatllCarInfo',
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options)
    this.data.carInfo.phone = options.phone
    this.setData({
      carInfo: this.data.carInfo
    })
    this.getsearchDistrict()
    // wx.navigateTo({
    //   url: '/pages/insatllCarInfo/insatllCarInfo',
    // })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})