// pages/insatllCarInfo/insatllCarInfo.js
import {
  simpleColours,
  rfTagLocations,
  searchDistrict,
  bikeSearch,
  rfidEntity
} from "../../request/getIndexData"
Page({

  /**
   * 页面的初始数据
   */
  data: {
    showSelectDistrictid: false,
    array: ['美国', '中国', '巴西', '日本'],
    index: 0,
    testimgurl: null,
    rfidlist: [],
    tagNum: '',
    carInfo: {
      vin: '',
      motorNumber: '',
      colour: [],
      productModel: {
        brandName: ''
      },
      licensePlate: '',
      ownerUrn: '',
      rfIdList: [{
        urn: '',
        imageUrl: '',
        rfTagLocation: '',
        idx: '0',
        img: ''
      }],
      snapshots: {
        LEFT: [],
        RIGHT: [],
        FRONT: [],
        BACK: [],
        BRAND: [],
        LICENSE: [],
        SIN_FRONT: [],
        SIN_BACK: [],
        ENGINE: [],
        VIN: []
      },
      snapshots2: {
        LEFT: [],
        RIGHT: [],
        FRONT: [],
        BACK: [],
        BRAND: [],
        LICENSE: [],
        SIN_FRONT: [],
        SIN_BACK: [],
        ENGINE: [],
        VIN: []
      },
      insurances: [{
        institute: "",
        imageUrl: "",
        img: "",
        comment: "",
        issued: `${new Date().getFullYear()}-${new Date().getMonth()+1}-${new Date().getDate()}`,
        start: "有效期开始时间",
        expired: "有效期结束时间",
        id: ""
      }]

    },
    carColorList: [],
    colorIndex: 0,
    rfTagList: [],
    rfIndex: 0,
    zhenarray: [],
    zhenindex: 0,
    selectZhen: '',
    suoarray: [],
    suoindex: 0,
  },
  carVin(e) {
    this.data.carInfo.vin = e.detail.value
  },
  vinunBlur(e) {
    let par = {
      vin: e.detail.value
    }
    bikeSearch(par).then(res => {
      if (res.data.results.length) {
        wx.showToast({
          title: '该车架号已经被登记',
          icon: 'none',
          duration: 2000
        })
        this.data.carInfo.vin = ''
        this.setData({
          carInfo: this.data.carInfo
        })
      }
    })
  },
  licensePlateUnblur(e) {
    let par = {
      licensePlate: e.detail.value
    }
    bikeSearch(par).then(res => {
      if (res.data.results.length) {
        wx.showToast({
          title: '该车牌号已经被登记',
          icon: 'none',
          duration: 2000
        })
        this.data.carInfo.licensePlate = ''
        this.setData({
          carInfo: this.data.carInfo
        })
      }
    })
  },
  carMotorNumber(e) {
    this.data.carInfo.motorNumber = e.detail.value
  },
  carLicensePlate(e) {
    this.data.carInfo.licensePlate = e.detail.value
  },
  carLicensePlate(e) {
    this.data.carInfo.licensePlate = e.detail.value
  },
  carProductModel(e) {
    this.data.carInfo.productModel.brandName = e.detail.value
  },
  getinstitute(e) {
    this.data.carInfo.insurances[e.target.dataset.index].institute = e.detail.value
  },
  getId(e) {
    this.data.carInfo.insurances[e.target.dataset.index].id = e.detail.value
  },
  bindDateChange: function (e) {
    this.data.carInfo.insurances[e.target.dataset.index].start = e.detail.value
    this.setData({
      carInfo: this.data.carInfo
    })
  },
  bindDatendChange: function (e) {
    // console.log('picker发送选择改变，携带值为', e.detail.value)
    // console.log(e)
    let start = new Date(this.data.carInfo.insurances[e.target.dataset.index].start).getTime()
    let end = new Date(e.detail.value).getTime()
    console.log(start)
    if (this.data.carInfo.insurances[e.target.dataset.index].start !== '有效期开始时间') {
      if (end > start) {
        this.data.carInfo.insurances[e.target.dataset.index].expired = e.detail.value
        this.setData({
          carInfo: this.data.carInfo
        })
      }else{
        wx.showToast({
          title: '结束时间大与开始时间',
          icon: 'none'
        })
      }
    } else {
      wx.showToast({
        title: '请先选择开始时间',
        icon: 'none'
      })
    }
  },
  getsimpleColours() {
    simpleColours().then(res => {
      res.data.unshift({
        text: "-请选择-"
      })
      this.setData({
        carColorList: res.data
      })
    })
  },
  showSimpleColour(e) {
    this.setData({
      colorIndex: e.detail.value
    })

    this.data.carInfo.colour = [this.data.carColorList[e.detail.value].code]
  },
  getrfTagLocations() {
    rfTagLocations().then(res => {
      res.data.unshift({
        text: "-请选择-"
      })
      this.setData({
        rfTagList: res.data
      })

    })
  },
  Changerfposition(e) {
    console.log(e)
    let index = e.currentTarget.dataset.index
    this.data.carInfo.rfIdList[index].rfTagLocation = this.data.rfTagList[e.detail.value].code
    this.data.carInfo.rfIdList[index].idx = e.detail.value
    this.setData({
      carInfo: this.data.carInfo
    })
  },
  getsearchDistrict() {
    let par = {
      parents: [
        "urn:bh:district:1"
      ]
    }
    searchDistrict(par).then(res => {
      res.data.results.unshift({
        name: "-请选择-"
      })
      this.setData({
        zhenarray: res.data.results
      })
    })
  },
  bindPickersuoChange(e) {
    let index = e.detail.value
    this.setData({
      suoindex: index
    })
    this.data.selectZhen = this.data.suoarray[index].urn
    console.log(this.data.suoarray[index].urn)
    wx.setStorageSync('locationName', `${this.data.zhenarray[this.data.zhenindex].name}_${this.data.suoarray[this.data.suoindex].name}`)
    wx.setStorageSync('districtId', this.data.suoarray[index].urn)
  },
  bindPickerChangezhen: function (e) {
    console.log('picker发送选择改变，携带值为', e)
    let index = e.detail.value
    this.setData({
      zhenindex: index
    })

    let par = {
      parents: [
        this.data.zhenarray[index].urn
      ]
    }
    wx.showLoading({
      title: '正在加载数据',
      mask: true
    })
    searchDistrict(par).then(res => {
      console.log(res)
      if (res.data.errors.length) {
        // wx.hideLoading({
        //   title: '加载失败，请重试',
        // })
      } else {

        this.setData({
          suoindex: 0
        })
        res.data.results.unshift({
          name: "-请选择-"
        })
        this.setData({
          suoarray: res.data.results
        })
        wx.hideLoading()
      }

    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

    let pages = getCurrentPages(); //页面对象
    let prevpage = pages[pages.length - 2]; //上一个页面对象
    console.log(prevpage.route) //上一个页面路由地址
    console.log(prevpage.route == 'pages/erectCar/erectCar')
    if (prevpage.route == 'pages/erectCar/erectCar') {
      this.setData({
        showSelectDistrictid: true
      })

    } else {
      this.setData({
        showSelectDistrictid: false
      })
    }

    this.getsearchDistrict()
    this.getsimpleColours()
    this.getrfTagLocations()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  //选择器变化监听
  bindPickerChange: function (e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      index: e.detail.value
    })
  },

  handleDeleteImg(e) {
    let d = e.currentTarget.dataset.current
    this.data.carInfo.rfIdList[d].imageUrl = ''
    this.data.carInfo.rfIdList[d].img = ''
    this.setData({
      carInfo: this.data.carInfo
    })
  },
  handleDeletebaodanImg(e) {
    let d = e.currentTarget.dataset.current
    this.data.carInfo.insurances[d].imageUrl = ''
    this.data.carInfo.insurances[d].img = ''
    this.setData({
      carInfo: this.data.carInfo
    })
  },
  getbaodanImage(e) {
    // let that = this;
    let index = e.currentTarget.dataset.index
    var that = this;

    wx.chooseImage({
      sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['camera'], // 可以指定来源是相册还是相机，默认二者都有
      success: function (res) {
        // 这里无论用户是从相册选择还是直接用相机拍摄，拍摄完成后的图片临时路径都会传递进来
        // app.startOperating("保存中")
        wx.showLoading({
          title: '图片上传中',
          mask: true
        })
        if (that.data.carInfo.insurances[index].imageUrl) {
          wx.showModal({
            title: '警告',
            content: "最多只能上传一张图片",
            showCancel: false
          })
        } else {
          let tempFilePaths = res.tempFilePaths

          that.data.carInfo.insurances[index].img = tempFilePaths[0]


          // that.setData({
          //   carInfo:that.data.carInfo
          // })
          wx.uploadFile({
            filePath: tempFilePaths[0],
            name: 'file',
            // url: `http://47.100.19.229:9001/public/api/sphyrnidae/v1/bike/image/upload?urn=` + that.data.carInfo.rfIdList[index].urn,
            url: `https://radar-backend.bearhunting.cn/sphyrnidae/v1/bike/image/upload?urn=urn:bh:bike:${that.data.carInfo.insurances[index].id}`,
            // url: `http://192.168.63.161:9001/public/api/sphyrnidae/v1/bike/image/upload?urn=`+that.data.carInfo.rfIdList[index].urn,
            header: {
              "Content-Type": "multipart/form-data",
            },
            success: function (res) {
              wx.hideLoading({
                title: '图片上传成功',
              })
              that.data.carInfo.insurances[index].imageUrl = JSON.parse(res.data).result
              that.setData({
                carInfo: that.data.carInfo
              })
              console.log(that.data.carInfo)
            },
            fail: function () {
              that.data.carInfo.insurances[index].img = ''
              wx.showToast({
                title: '图片上传失败',
                icon: 'error',
                duration: 2000
              })
            }
          })
        }
        // let arr = that.data.rfidlist;
        // arr[id].imgurl = res.tempFilePaths[0];

        that.setData({
          carInfo: that.data.carInfo
        })
        // var filePath=res.tempFilePaths[0];
        // var session_key=wx.getStorageSync('session_key');
        // 这里顺道展示一下如何将上传上来的文件返回给后端，就是调用wx.uploadFile函数
        // wx.uploadFile({
        //   url: app.globalData.url+'/home/upload/uploadFile/session_key/'+session_key,
        //   filePath: filePath,
        //   name: 'file',
        //   success:function(res){
        //     app.stopOperating();
        //     // 下面的处理其实是跟我自己的业务逻辑有关
        //     var data=JSON.parse(res.data);
        //     if(parseInt(data.status)===1){
        //       app.showSuccess('文件保存成功');
        //     }else{
        //       app.showError("文件保存失败");
        //     }
        //   }
        // })
      },
      fail: function (error) {
        console.error("调用本地相册文件时出错")
        console.warn(error)
      },
      complete: function () {}
    })
  },
  // 拍照功能
  getLocalImage: function (e) {
    // let that = this;
    let index = e.currentTarget.dataset.index
    var that = this;

    wx.chooseImage({
      sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['camera'], // 可以指定来源是相册还是相机，默认二者都有
      success: function (res) {
        // 这里无论用户是从相册选择还是直接用相机拍摄，拍摄完成后的图片临时路径都会传递进来
        // app.startOperating("保存中")
        wx.showLoading({
          title: '图片上传中',
          mask: true
        })
        if (that.data.carInfo.rfIdList[index].imageUrl) {
          wx.showModal({
            title: '警告',
            content: "最多只能上传一张图片",
            showCancel: false
          })
        } else {
          let tempFilePaths = res.tempFilePaths

          that.data.carInfo.rfIdList[index].img = tempFilePaths[0]


          // that.setData({
          //   carInfo:that.data.carInfo
          // })
          wx.uploadFile({
            filePath: tempFilePaths[0],
            name: 'file',
            // url: `http://47.100.19.229:9001/public/api/sphyrnidae/v1/bike/image/upload?urn=` + that.data.carInfo.rfIdList[index].urn,
            url: `https://radar-backend.bearhunting.cn/sphyrnidae/v1/bike/image/upload?urn=` + that.data.carInfo.rfIdList[index].urn,
            // url: `http://192.168.63.161:9001/public/api/sphyrnidae/v1/bike/image/upload?urn=`+that.data.carInfo.rfIdList[index].urn,
            header: {
              "Content-Type": "multipart/form-data",
            },
            success: function (res) {
              wx.hideLoading({
                title: '图片上传成功',
              })
              that.data.carInfo.rfIdList[index].imageUrl = JSON.parse(res.data).result
              that.setData({
                carInfo: that.data.carInfo
              })
              console.log(that.data.carInfo)
            },
            fail: function () {
              that.data.carInfo.rfIdList[index].img = ''
              wx.showToast({
                title: '图片上传失败',
                icon: 'error',
                duration: 2000
              })
            }
          })
        }
        // let arr = that.data.rfidlist;
        // arr[id].imgurl = res.tempFilePaths[0];

        that.setData({
          carInfo: that.data.carInfo
        })
        // var filePath=res.tempFilePaths[0];
        // var session_key=wx.getStorageSync('session_key');
        // 这里顺道展示一下如何将上传上来的文件返回给后端，就是调用wx.uploadFile函数
        // wx.uploadFile({
        //   url: app.globalData.url+'/home/upload/uploadFile/session_key/'+session_key,
        //   filePath: filePath,
        //   name: 'file',
        //   success:function(res){
        //     app.stopOperating();
        //     // 下面的处理其实是跟我自己的业务逻辑有关
        //     var data=JSON.parse(res.data);
        //     if(parseInt(data.status)===1){
        //       app.showSuccess('文件保存成功');
        //     }else{
        //       app.showError("文件保存失败");
        //     }
        //   }
        // })
      },
      fail: function (error) {
        console.error("调用本地相册文件时出错")
        console.warn(error)
      },
      complete: function () {}
    })
  },
  addInsurances() {
    let arr = this.data.carInfo.insurances;
    let Ir = {
      institute: "",
      imageUrl: "",
      img: "",
      comment: "",
      issued: `${new Date().getFullYear()}-${new Date().getMonth()+1}-${new Date().getDate()}`,
      start: "有效期开始时间",
      expired: "有效期结束时间",
      id: ""
    }
    arr.push(Ir);
    this.setData({
      carInfo: this.data.carInfo
    })
  },
  deleteInsu(e) {
    let index = e.currentTarget.dataset.index
    let arr = this.data.carInfo.insurances;
    arr.splice(index, 1)
    this.setData({
      carInfo: this.data.carInfo
    })
  },
  addRfid() {
    let arr = this.data.carInfo.rfIdList;
    let rf = {
      urn: "",
      imageUrl: "",
      rfTagLocation: "",
      idx: '0',
      img: ''
    }
    arr.push(rf);
    this.setData({
      carInfo: this.data.carInfo
    })
  },
  deleteRfid(e) {
    console.log(e)
    let index = e.currentTarget.dataset.index
    let arr = this.data.carInfo.rfIdList;
    arr.splice(index, 1)
    this.setData({
      carInfo: this.data.carInfo
    })
  },
  nextTOpHOTO() {
    console.log(this.data.carInfo)
    if (!this.data.carInfo.productModel.brandName) {
      wx.showToast({
        title: '请填写品牌',
        icon: 'none',
        duration: 2000
      })
      return false;
    }
    if (!this.data.carInfo.motorNumber) {
      wx.showToast({
        title: '请填写电机号',
        icon: 'none',
        duration: 2000
      })
      return false;
    }
    if (!this.data.carInfo.vin) {
      wx.showToast({
        title: '请填写车架号',
        icon: 'none',
        duration: 2000
      })
      return false;
    }
    if (!this.data.carInfo.licensePlate) {
      wx.showToast({
        title: '请填写车牌号',
        icon: 'none',
        duration: 2000
      })
      return false;
    }
    for (let i = 0; i < this.data.carInfo.rfIdList.length; i++) {
      if (!this.data.carInfo.rfIdList[i].rfTagLocation) {
        wx.showToast({
          title: '请选择标签安装位置',
          icon: 'none',
          duration: 2000
        })
        return false;
      }
      if (!this.data.carInfo.rfIdList[i].urn) {
        wx.showToast({
          title: '请扫描标签号',
          icon: 'none',
          duration: 2000
        })
        return false;
      }
      if (!this.data.carInfo.rfIdList[i].imageUrl) {
        wx.showToast({
          title: '请拍下标签安装位置',
          icon: 'none',
          duration: 2000
        })
        return false;
      }
    }
    for (let i = 0; i < this.data.carInfo.insurances.length; i++) {
      if (!this.data.carInfo.insurances[i].id) {
        wx.showToast({
          title: '请填写证书编号',
          icon: 'none',
          duration: 2000
        })
        return false;
      }
      if (!this.data.carInfo.insurances[i].institute) {
        wx.showToast({
          title: '请填写签发机构',
          icon: 'none',
          duration: 2000
        })
        return false;
      }
      if (!this.data.carInfo.insurances[i].start) {
        wx.showToast({
          title: '请选择有效期开始时间',
          icon: 'none',
          duration: 2000
        })
        return false;
      }
      if (!this.data.carInfo.insurances[i].expired) {
        wx.showToast({
          title: '请选择有效期结束时间',
          icon: 'none',
          duration: 2000
        })
        return false;
      }
      if (!this.data.carInfo.insurances[i].imageUrl) {
        wx.showToast({
          title: '请拍摄保单照片',
          icon: 'none',
          duration: 2000
        })
        return false;
      }
    }
    // if(!this.data.carInfo.licensePlate){
    //   wx.showToast({
    //     title: '请填写车牌号',
    //     icon: 'none',
    //     duration: 2000
    //   })
    //   return false;
    // }
    if (wx.getStorageSync('districtId')) {
      wx.setStorageSync('eleBikeInfo', JSON.stringify(this.data.carInfo))
      wx.navigateTo({
        url: '/pages/carInfo/carInfo',
      })
    } else {
      wx.showModal({
        title: '提示',
        content: "请先选择所在区域",
        showCancel: false
      })
      return false;
    }

  },
  scancode: function (e) {
    // 允许从相机和相册扫码
    let that = this
    wx.scanCode({
      success(res) {
        console.log(Number(res.result));
        let index = e.currentTarget.dataset.index
        that.data.carInfo.rfIdList[index].urn = `urn:bh:rfid:${Number(res.result)}`
        rfidEntity({
          key: `urn:bh:rfid:${Number(res.result)}`
        }).then(res => {
          if (res.data.error) {
            wx.showToast({
              title: '标签号不存在，请重扫',
              icon: 'none',
              duration: 2000
            })
          } else {
            that.setData({
              carInfo: that.data.carInfo
            })
            wx.showToast({
              title: '成功',
              icon: 'success',
              duration: 2000
            })
          }

        })
      },
      fail: (res) => {
        console.log(res);
        wx.showToast({
          title: '扫描失败',
          icon: 'none',
          duration: 2000
        })
      }
    })
  }






})