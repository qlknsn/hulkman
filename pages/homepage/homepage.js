// pages/homepage/homepage.js
import {
  searchDistrict,
  subscriberMembers,
  getopenId,
  login
} from "../../request/getIndexData"
let app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    array: ['美国', '中国', '巴西', '日本'],
    index: 0,
    zhenarray: [],
    zhenindex: 0,
    selectZhen: '',
    suoarray: [],
    suoindex: 0,
    searchUserList: []
  },
  getsearchDistrict() {
    let par = {
      parents: [
        "urn:bh:district:1"
      ]
    }
    searchDistrict(par).then(res => {
      if (res.data.errors && res.data.errors.code == 401) {
        wx.showToast({
          title: res.data.error.message,
        })
        setTimeout(() => {
          wx.reLaunch({
            url: '/pages/register/register',
            icon: 'none'
          })
        }, 1000)
      } else {
        res.data.results.unshift({
          name: "选择街镇"
        })
        this.setData({
          zhenarray: res.data.results
        })
      }
    })
  },
  getmember(e) {
    this.setData({
      currentMember: e.detail.value
    })

    if (e.detail.value == '') {
      this.setData({
        searchUserList: []
      })

    }
  },
  toErectCar(e) {
    console.log(e)
    let item = e.currentTarget.dataset.item
    wx.navigateTo({
      url: '/pages/erectCar/erectCar?userUrn=' + item.urn + '&phone=' + item.member.phones[0],
    })
  },
  checkmember() {
    let par = {
      phone: this.data.currentMember
    }
    if (this.data.currentMember) {
      wx.showLoading({
        title: '查询中',
        mask: true
      })
    }

    // if (wx.getStorageSync('districtId')) {
    if (this.data.currentMember) {
      subscriberMembers(par).then(res => {
        wx.hideLoading()
        if (res.data.results.length) {

          this.setData({
            searchUserList: res.data.results
          })
        } else {
          wx.showToast({
            title: '该车主无电动车，请点击新增车主按钮儿',
            icon: 'none',
            duration: 2000
          })
        }
      })
    } else {
      wx.showToast({
        title: "请输入身份证信息",
        icon: 'none',
        duration: 3000
      })
    }
    // } else {
    //   wx.showModal({
    //     title: '提示',
    //     content: "请先选择所在区域",
    //     showCancel: false
    //   })
    // }

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },



  wxlogin() {
    var that = this;
    wx.login({
      success(res) {
        if (res.code) {
          console.log(res);
          let params = {
            code: res.code,
          }
          getopenId(params).then(res => {
            let par = {
              openId: res.data.result.openid,
            }
            console.log(res.data.result)
            app.globalData.sesszionKey = res.data.result.sessionKey
            app.globalData.openId = res.data.result.openid
            // wx.setStorageSync('sesszionKey', res.data.result.sessionKey)
            login(par).then(res => {

              // if (res.data.status == 404) {
              //   wx.navigateTo({
              //     url: '/pages/register/register',
              //   })
              // } else {
                if (res.data.result.auth.token) {
                  let cookie = res.data.result.auth.token
                  console.log(cookie)
                  wx.setStorageSync("cookieInfo", cookie)
                  // wx.reLaunch({
                  //   // url: '/pages/user/user',
                  //   url: '/pages/homepage/homepage',
                  //   // url: '/pages/bluetooth/bluetooth',
                  // })
                } else {
                  wx.navigateTo({
                    url: '/pages/register/register',
                  })
                }
              // }
            })
          })

        } else {
          console.log("登录失败");
        }
      }
    })
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.wxlogin();
    this.getsearchDistrict()
    wx.removeStorage({
      key: 'districtId',
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },



  //选择器变化监听
  bindPickerChange: function (e) {
    console.log('picker发送选择改变，携带值为', e)
    let index = e.detail.value
    this.setData({
      zhenindex: index
    })

    let par = {
      parents: [
        this.data.zhenarray[index].urn
      ]
    }
    wx.showLoading({
      title: '正在加载数据',
      mask: true
    })
    searchDistrict(par).then(res => {
      console.log(res)
      if (res.data.errors.length) {
        // wx.hideLoading({
        //   title: '加载失败，请重试',
        // })
      } else {

        this.setData({
          suoindex: 0
        })
        res.data.results.unshift({
          name: "选择安装点"
        })
        this.setData({
          suoarray: res.data.results
        })
        wx.hideLoading()
      }

    })
  },
  bindPickersuoChange(e) {
    let index = e.detail.value
    this.setData({
      suoindex: index
    })
    this.data.selectZhen = this.data.suoarray[index].urn
    console.log(this.data.zhenarray[this.data.zhenindex].name, this.data.suoarray[this.data.suoindex].name)
    wx.setStorageSync('locationName', `${this.data.zhenarray[this.data.zhenindex].name}_${this.data.suoarray[this.data.suoindex].name}`)
    wx.setStorageSync('districtId', this.data.suoarray[index].urn)
  },
  register() {
    if (wx.getStorageSync('districtId')) {
      let par = {
        phone: this.data.currentMember
      }
      if (this.data.currentMember&&this.data.currentMember.length==11) {
        subscriberMembers(par).then(res => {
          if (res.data.results.length) {
            wx.showToast({
              title: '该车主已经登记请进行验证',
              icon: 'none',
              duration: 2000
            })
          } else {
            wx.navigateTo({
              url: '/pages/carownerinfo/carownerinfo?phone=' + this.data.currentMember,
              // url: '/pages/carInfo/carInfo',
            })
          }
        })
      } else {
        wx.showToast({
          title: "请输入正确手机号",
          icon: 'none',
          duration: 3000
        })
      }
    } else {
      wx.showToast({
        title: '请先选择所在区域',
        icon: 'none',
        duration: 3000
      })
    }
  }
})