// pages/insatllCarInfo/insatllCarInfo.js
import {
  simpleColours,
  rfTagLocations,
  uploadImg,
  singlebikeEntity,
  editBikeEntities,
  getopenId,
  login,
  rfidEntity
} from "../../request/getIndexData"
Page({

  /**
   * 页面的初始数据
   */
  data: {
    modalHidden: true,
    array: ['美国', '中国', '巴西', '日本'],
    index: 0,
    testimgurl: null,
    rfidlist: [],
    tagNum: '',
    carInfo: {
      vin: '',
      motorNumber: '',
      colour: [],
      productModel: {
        brandName: ''
      },
      licensePlate: '',
      ownerUrn: '',
      rfIdList: [{
        urn: '',
        imageUrl: '',
        rfTagLocation: '',
        idx: '0',
        img: ''
      }],
      snapshots: {
        LEFT: [],
        RIGHT: [],
        FRONT: [],
        BACK: [],
        BRAND: [],
        LICENSE: [],
        SIN_FRONT: [],
        SIN_BACK: [],
        ENGINE: [],
        VIN: []
      },
      snapshots2: {
        LEFT: [],
        RIGHT: [],
        FRONT: [],
        BACK: [],
        BRAND: [],
        LICENSE: [],
        SIN_FRONT: [],
        SIN_BACK: [],
        ENGINE: [],
        VIN: []
      },
      insurances: [{
        institute: "",
        imageUrl: "",
        img: "",
        comment: "",
        issued: `${new Date().getFullYear()}-${new Date().getMonth()+1}-${new Date().getDate()}`,
        start: "有效期开始时间",
        expired: "有效期结束时间",
        id: ""
      }]
    },
    carColorList: [],
    colorIndex: 0,
    rfTagList: [],
    rfIndex: 0,
    getBickInfo: {},
    IMG_HOST: 'https://bh-radar.oss-cn-shanghai.aliyuncs.com/'
  },
  /**
   * 显示弹窗
   */
  buttonTap: function () {
    this.setData({
      modalHidden: false
    })
  },

  /**
   * 点击取消
   */
  modalCandel: function () {
    // do something
    this.setData({
      modalHidden: true
    })
  },
  chooseImg(e) {
    let type = e.currentTarget.dataset.type
    let that = this
    wx.chooseImage({
      count: 1,
      sizeType: ['original', 'compressed'],
      sourceType: ['camera'],
      success(res) {

        wx.showLoading({
          title: '图片上传中',
          mask:true
        })
        let tempFilePaths = res.tempFilePaths
        console.log(tempFilePaths[0])
        that.data.getBickInfo.snapshots2[type] = [tempFilePaths[0]]
        // tempFilePath可以作为img标签的src属性显示图片
        that.setData({
          getBickInfo: that.data.getBickInfo
        })
        let urn = `urn:bh:bike:${that.data.getBickInfo.licensePlate}:${that.data.getBickInfo.vin}`
        wx.uploadFile({
          filePath: tempFilePaths[0],
          name: 'file',
          // url: `http://47.100.19.229:9001/public/api/sphyrnidae/v1/bike/image/upload?urn=` + urn,
          url: `https://radar-backend.bearhunting.cn/sphyrnidae/v1/bike/image/upload?urn=` + urn,
          // url: `http://192.168.63.161:9001/public/api/sphyrnidae/v1/bike/image/upload?urn=` + urn,
          header: {
            "Content-Type": "multipart/form-data",
          },
          success: function (res) {
            wx.hideLoading({
              title: '图片上传成功',
            })
            that.data.getBickInfo.snapshots[type] = [{
              url: JSON.parse(res.data).result
            }]
            that.setData({
              getBickInfo: that.data.getBickInfo
            })
          },
          fail:function(){
            that.data.getBickInfo.snapshots2[type] = []
            wx.showToast({
              title: '图片上传失败',
              icon: 'error',
              duration: 2000
            })
          }
        })
      }
    })
  },
  closeCurrent(e) {
    let type = e.currentTarget.dataset.type
    this.data.getBickInfo.snapshots[type] = []
    this.data.getBickInfo.snapshots2[type] = []
    this.setData({
      getBickInfo: this.data.getBickInfo
    })
  },


  carVin(e) {
    this.data.getBickInfo.vin = e.detail.value
  },
  carMotorNumber(e) {
    this.data.getBickInfo.motorNumber = e.detail.value
  },
  carLicensePlate(e) {
    this.data.getBickInfo.licensePlate = e.detail.value
  },
  carProductModel(e) {
    this.data.getBickInfo.productModel.brandName = e.detail.value
  },
  getId(e) {
    this.data.getBickInfo.insurances[e.target.dataset.index].id = e.detail.value
  },
  getinstitute(e) {
    this.data.getBickInfo.insurances[e.target.dataset.index].institute = e.detail.value
  },
  bindDateChange: function (e) {
    this.data.getBickInfo.insurances[e.target.dataset.index].start = e.detail.value
    this.setData({
      getBickInfo: this.data.getBickInfo
    })
  },
  bindDatendChange: function (e) {
    // console.log('picker发送选择改变，携带值为', e.detail.value)
    // console.log(e)
    let start = new Date(this.data.getBickInfo.insurances[e.target.dataset.index].start).getTime()
    let end = new Date(e.detail.value).getTime()
    // this.data.getBickInfo.insurances[e.target.dataset.index].expired = e.detail.value
    // this.setData({
    //   getBickInfo: this.data.getBickInfo
    // })
    if (this.data.getBickInfo.insurances[e.target.dataset.index].start !== '有效期开始时间') {
      if (end > start) {
        this.data.getBickInfo.insurances[e.target.dataset.index].expired = e.detail.value
        this.setData({
          getBickInfo: this.data.getBickInfo
        })
      }else{
        wx.showToast({
          title: '结束时间大与开始时间',
          icon: 'none'
        })
      }
    } else {
      wx.showToast({
        title: '请先选择开始时间',
        icon: 'none'
      })
    }
  },
  getsimpleColours() {
    simpleColours().then(res => {
      res.data.unshift({
        text: "-请选择-"
      })
      this.setData({
        carColorList: res.data
      })
    })
  },
  showSimpleColour(e) {
    this.setData({
      colorIndex: e.detail.value
    })

    this.data.getBickInfo.colour = [this.data.carColorList[e.detail.value].code]
  },
  getrfTagLocations() {
    rfTagLocations().then(res => {
      res.data.unshift({
        text: "-请选择-"
      })
      this.setData({
        rfTagList: res.data
      })

    })
  },
  Changerfposition(e) {
    console.log(e)
    let index = e.currentTarget.dataset.index
    this.data.getBickInfo.rfIdList[index].rfTagLocation = this.data.rfTagList[e.detail.value].code
    this.data.getBickInfo.rfIdList[index].idx = e.detail.value
    this.setData({
      getBickInfo: this.data.getBickInfo
    })
  },
  getSingleBike(par) {
    singlebikeEntity(par).then(res => {
      this.setData({
        getBickInfo: res.data.result
      })
      let index = this.data.carColorList.findIndex((item) => {
        return item.code == res.data.result.colour[0]
      })
      this.data.getBickInfo.rfIdList.forEach((res) => {
        this.data.rfTagList.findIndex((item, index) => {
          if (res.rfTagLocation == item.code) {
            res.idx = index
          }
        })
      })
      this.data.getBickInfo.snapshots2 = JSON.parse(JSON.stringify(this.data.getBickInfo.snapshots))
      // let i = this.data.rfTagList.findIndex((item)=>{
      //   return item.code == res.data.result.rfIdList[0].rfTagLocation
      // })
      this.setData({
        colorIndex: index
      })

      this.setData({
        getBickInfo: this.data.getBickInfo
      })
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      modalHidden: false
    })
    this.getsimpleColours()
    this.getrfTagLocations()
    let par = {
      urn: options.urn
    }
    this.getSingleBike(par)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  addInsurances() {
    let arr = this.data.getBickInfo.insurances;
    let Ir = {
      institute: "",
      imageUrl: "",
      img: "",
      comment: "",
      issued: `${new Date().getFullYear()}-${new Date().getMonth()+1}-${new Date().getDate()}`,
      start: "有效期开始时间",
      expired: "有效期结束时间",
      id: ""
    }
    arr.push(Ir);
    this.setData({
      getBickInfo: this.data.getBickInfo
    })
  },
  deleteInsu(e) {
    let index = e.currentTarget.dataset.index
    let arr = this.data.getBickInfo.insurances;
    arr.splice(index, 1)
    this.setData({
      getBickInfo: this.data.getBickInfo
    })
  },
  //选择器变化监听
  bindPickerChange: function (e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      index: e.detail.value
    })
  },

  handleDeleteImg(e) {
    let d = e.currentTarget.dataset.current
    this.data.getBickInfo.rfIdList[d].imageUrl = ''
    this.data.getBickInfo.rfIdList[d].img = ''
    this.setData({
      getBickInfo: this.data.getBickInfo
    })
  },
  handleDeletebaodanImg(e) {
    let d = e.currentTarget.dataset.current
    this.data.getBickInfo.insurances[d].imageUrl = ''
    this.data.getBickInfo.insurances[d].img = ''
    this.setData({
      getBickInfo: this.data.getBickInfo
    })
  },
  getbaodanImage(e) {
    // let that = this;
    let index = e.currentTarget.dataset.index
    var that = this;

    wx.chooseImage({
      sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['camera'], // 可以指定来源是相册还是相机，默认二者都有
      success: function (res) {
        // 这里无论用户是从相册选择还是直接用相机拍摄，拍摄完成后的图片临时路径都会传递进来
        // app.startOperating("保存中")
        wx.showLoading({
          title: '图片上传中',
          mask: true
        })
        if (that.data.getBickInfo.insurances[index].imageUrl) {
          wx.showModal({
            title: '警告',
            content: "最多只能上传一张图片",
            showCancel: false
          })
        } else {
          let tempFilePaths = res.tempFilePaths

          that.data.getBickInfo.insurances[index].img = tempFilePaths[0]


          // that.setData({
          //   carInfo:that.data.carInfo
          // })
          wx.uploadFile({
            filePath: tempFilePaths[0],
            name: 'file',
            // url: `http://47.100.19.229:9001/public/api/sphyrnidae/v1/bike/image/upload?urn=` + that.data.carInfo.rfIdList[index].urn,
            url: `https://radar-backend.bearhunting.cn/sphyrnidae/v1/bike/image/upload?urn=urn:bh:bike:${that.data.carInfo.insurances[index].id}`,
            // url: `http://192.168.63.161:9001/public/api/sphyrnidae/v1/bike/image/upload?urn=`+that.data.carInfo.rfIdList[index].urn,
            header: {
              "Content-Type": "multipart/form-data",
            },
            success: function (res) {
              wx.hideLoading({
                title: '图片上传成功',
              })
              that.data.getBickInfo.insurances[index].imageUrl = JSON.parse(res.data).result
              that.setData({
                getBickInfo: that.data.getBickInfo
              })
              console.log(that.data.getBickInfo)
            },
            fail: function () {
              that.data.getBickInfo.insurances[index].img = ''
              wx.showToast({
                title: '图片上传失败',
                icon: 'error',
                duration: 2000
              })
            }
          })
        }
        // let arr = that.data.rfidlist;
        // arr[id].imgurl = res.tempFilePaths[0];

        that.setData({
          getBickInfo: that.data.getBickInfo
        })
        // var filePath=res.tempFilePaths[0];
        // var session_key=wx.getStorageSync('session_key');
        // 这里顺道展示一下如何将上传上来的文件返回给后端，就是调用wx.uploadFile函数
        // wx.uploadFile({
        //   url: app.globalData.url+'/home/upload/uploadFile/session_key/'+session_key,
        //   filePath: filePath,
        //   name: 'file',
        //   success:function(res){
        //     app.stopOperating();
        //     // 下面的处理其实是跟我自己的业务逻辑有关
        //     var data=JSON.parse(res.data);
        //     if(parseInt(data.status)===1){
        //       app.showSuccess('文件保存成功');
        //     }else{
        //       app.showError("文件保存失败");
        //     }
        //   }
        // })
      },
      fail: function (error) {
        console.error("调用本地相册文件时出错")
        console.warn(error)
      },
      complete: function () {}
    })
  },
  // 拍照功能
  getLocalImage: function (e) {
    // let that = this;
    let index = e.currentTarget.dataset.index
    var that = this;
    wx.chooseImage({
      sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['camera'], // 可以指定来源是相册还是相机，默认二者都有
      success: function (res) {
        // 这里无论用户是从相册选择还是直接用相机拍摄，拍摄完成后的图片临时路径都会传递进来
        // app.startOperating("保存中")
        wx.showLoading({
          title: '图片上传中',
          mask:true
        })
        if (that.data.getBickInfo.rfIdList[index].imageUrl) {
          wx.showModal({
            title: '警告',
            content: "最多只能上传一张图片",
            showCancel: false
          })
        } else {
          let tempFilePaths = res.tempFilePaths
          console.log(tempFilePaths[0])
          that.data.getBickInfo.rfIdList[index].img = tempFilePaths[0]

          // that.setData({
          //   carInfo:that.data.carInfo
          // })
          wx.uploadFile({
            filePath: tempFilePaths[0],
            name: 'file',
            // url: `http://47.100.19.229:9001/public/api/sphyrnidae/v1/bike/image/upload?urn=` + that.data.getBickInfo.rfIdList[index].urn,
            url: `https://radar-backend.bearhunting.cn/sphyrnidae/v1/bike/image/upload?urn=` + that.data.getBickInfo.rfIdList[index].urn,
            // url: `http://192.168.63.161:9001/public/api/sphyrnidae/v1/bike/image/upload?urn=` + that.data.getBickInfo.rfIdList[index].urn,
            header: {
              "Content-Type": "multipart/form-data",
            },
            success: function (res) {
              wx.hideLoading({
                title: '图片上传成功',
              })
              that.data.getBickInfo.rfIdList[index].imageUrl = JSON.parse(res.data).result
              that.setData({
                getBickInfo: that.data.getBickInfo
              })
              console.log(that.data.getBickInfo)
            },
            fail:function(){
              that.data.getBickInfo.rfIdList[d].img = ''
              wx.showToast({
                title: '图片上传失败',
                icon: 'error',
                duration: 2000
              })
            }
          })
        }
        // let arr = that.data.rfidlist;
        // arr[id].imgurl = res.tempFilePaths[0];

        that.setData({
          getBickInfo: that.data.getBickInfo
        })
        // var filePath=res.tempFilePaths[0];
        // var session_key=wx.getStorageSync('session_key');
        // 这里顺道展示一下如何将上传上来的文件返回给后端，就是调用wx.uploadFile函数
        // wx.uploadFile({
        //   url: app.globalData.url+'/home/upload/uploadFile/session_key/'+session_key,
        //   filePath: filePath,
        //   name: 'file',
        //   success:function(res){
        //     app.stopOperating();
        //     // 下面的处理其实是跟我自己的业务逻辑有关
        //     var data=JSON.parse(res.data);
        //     if(parseInt(data.status)===1){
        //       app.showSuccess('文件保存成功');
        //     }else{
        //       app.showError("文件保存失败");
        //     }
        //   }
        // })
      },
      fail: function (error) {
        console.error("调用本地相册文件时出错")
        console.warn(error)
      },
      complete: function () {}
    })
  },
  addRfid() {
    let arr = this.data.getBickInfo.rfIdList;
    let rf = {
      urn: "",
      imageUrl: "",
      rfTagLocation: "",
      idx: '0',
      img: ''
    }
    arr.push(rf);
    this.setData({
      getBickInfo: this.data.getBickInfo
    })
  },
  deleteRfid(e){
    console.log(e)
    let index = e.currentTarget.dataset.index
    let arr = this.data.getBickInfo.rfIdList;
    arr.splice(index,1)
    this.setData({
      getBickInfo: this.data.getBickInfo
    })
  },
  editBike() {
    if(this.data.getBickInfo.ownerName){
      this.data.getBickInfo.ownerName;
    }else{
      this.data.getBickInfo.ownerName = wx.getStorageSync('ownerName')
    }
    console.log(this.data.getBickInfo.rfIdList.length>0)
    if(this.data.getBickInfo.rfIdList.length>0){
      this.data.getBickInfo.rfIdList;
    }else{
      wx.showToast({
        title: '请至少添加一个标签',
        icon: 'none',
        duration: 2000
      })
      return false;
    }

    for (let i = 0; i < this.data.getBickInfo.rfIdList.length; i++) {
      if (!this.data.getBickInfo.rfIdList[i].rfTagLocation) {
        wx.showToast({
          title: '请选择标签安装位置',
          icon: 'none',
          duration: 2000
        })
        return false;
      }
      if (!this.data.getBickInfo.rfIdList[i].urn) {
        wx.showToast({
          title: '请扫描标签号',
          icon: 'none',
          duration: 2000
        })
        return false;
      }
      if (!this.data.getBickInfo.rfIdList[i].imageUrl) {
        wx.showToast({
          title: '请拍下标签安装位置',
          icon: 'none',
          duration: 2000
        })
        return false;
      }
    }
    if(this.data.getBickInfo.snapshots.FRONT.length==0){
      wx.showToast({
        title: '请拍车头照片',
        icon: 'none',
        duration: 2000
      })
      return false;
    }
    if(this.data.getBickInfo.snapshots.LEFT.length==0){
      wx.showToast({
        title: '请拍车辆侧身照片',
        icon: 'none',
        duration: 2000
      })
      return false;
    }
    if(this.data.getBickInfo.snapshots.LICENSE.length==0){
      wx.showToast({
        title: '请拍车辆牌照照片',
        icon: 'none',
        duration: 2000
      })
      return false;
    }
    if(this.data.getBickInfo.snapshots.BACK.length==0){
      wx.showToast({
        title: '请拍车尾照片',
        icon: 'none',
        duration: 2000
      })
      return false;
    }
    if(this.data.getBickInfo.snapshots.SIN_FRONT.length==0){
      wx.showToast({
        title: '请拍车主身份证正面照片',
        icon: 'none',
        duration: 2000
      })
      return false;
    }
    if(this.data.getBickInfo.snapshots.SIN_BACK.length==0){
      wx.showToast({
        title: '请拍车主身份证反面照片',
        icon: 'none',
        duration: 2000
      })
      return false;
    }
    if(this.data.getBickInfo.snapshots.ENGINE.length==0){
      wx.showToast({
        title: '请拍电机号照片',
        icon: 'none',
        duration: 2000
      })
      return false;
    }
    if(this.data.getBickInfo.snapshots.VIN.length==0){
      wx.showToast({
        title: '请拍车架号照片',
        icon: 'none',
        duration: 2000
      })
      return false;
    }
    editBikeEntities([this.data.getBickInfo]).then(res => {
      if (res.data.error) {
        if (res.data.error.code == 401) {
          let that = this
          wx.login({
            success(res) {
              if (res.code) {
                let params = {
                  code: res.code,
                }
                getopenId(params).then(res => {
                  let par = {
                    openId: res.data.result.openid,
                  }
                  login(par).then(res => {
                    if (res.data.result.auth) {
                      let cookie = res.data.result.auth.token
                      wx.setStorageSync("cookieInfo", cookie)
                      wx.showToast({
                        title: '请重新提交',
                        icon: 'none',
                        duration: 2000
                      })
                    } else {
                      wx.showToast({
                        title: '登录信息失效请退出重进',
                        icon: 'none',
                        duration: 2000
                      })
                    }
                  })
                })

              } else {
                wx.showToast({
                  title: '登录信息失效请退出重进',
                  icon: 'none',
                  duration: 2000
                })
              }
            }
          })
        }
      } else {
        wx.showToast({
          title: '更新信息成功',
          icon: 'none',
          duration: 2000
        })
        setTimeout(()=>{
          wx.reLaunch({
            url: '/pages/erectCar/erectCar?userUrn=' + res.data.results[0].ownerUrn + '&phone=' + res.data.results[0].ownerUrn.replace('urn:cn:member:phone:', ''),
          })
        },1000)
      }
    })
  },
  nextTOpHOTO() {
    console.log(this.data.getBickInfo)
    // wx.setStorageSync('eleBikeInfo', JSON.stringify(this.data.carInfo))
    this.editBike()
  },
  scancode: function (e) {
    // 允许从相机和相册扫码
    let that = this
    wx.scanCode({
      success(res) {
        console.log(res);
        console.log(Number(res.result));
        let index = e.currentTarget.dataset.index
        that.data.getBickInfo.rfIdList[index].urn = `urn:bh:rfid:${Number(res.result)}`
        rfidEntity({
          key: `urn:bh:rfid:${Number(res.result)}`
        }).then(res => {
          if (res.data.error) {
            wx.showToast({
              title: '标签号不存在，请重扫',
              icon: 'none',
              duration: 2000
            })
          } else {
            that.setData({
              getBickInfo: that.data.getBickInfo
            })
            wx.showToast({
              title: '成功',
              icon: 'success',
              duration: 2000
            })
          }

        })
        

      },
      fail: (res) => {
        console.log(res);
        wx.showToast({
          title: '失败',
          icon: 'none',
          duration: 2000
        })
      }
    })
  }
})