var app = getApp();
var gbks = require('../../utils/transfer.js');
Page({
  data: {
    currentDevName: 'defaulName',
    time: '1000',
    currentConnectionState: '连接中...',
    recvTotalBytes: '0',
    recvCrc32: '0',
    sendTotalBytesCounter: 0,
    sendTotalBytes: '0',
    sendCrc32: '0',
    detail: false,
    recv: '',
    recv_data: '',
    recv_data_hex: '',
    scrollTop: '',
    progress: 0,
    send_data: '',
    interval: '',
    txd: [],
    delay: '',
    delayfile: '',
    hex: false,
    devId: '123',
    datalist: [],
    options: '',
    sign: 1,
    unload: false,
    services: [
      '22',
    ],
    connected: false,

    notifyServiceId: 'invalid',
    notifyCharacteristicId: 'invalid',
    notifyServiceSearchIndex: 0,

    writeServiceId: 'invalid',
    writeCharacteristicId: 'invalid',
    writeServiceSearchIndex: 0,


    getBikeInfo: {}
  },
  seekFirstNotifyCharacteristic: function () {
    var that = this;

    if (that.data.notifyServiceSearchIndex < that.data.services.length && that.data.notifyCharacteristicId == 'invalid') {
      that.data.notifyServiceId = that.data.services[that.data.notifyServiceSearchIndex].uuid
      console.log('Search service index ', that.data.notifyServiceSearchIndex, 'service: ', that.data.notifyServiceId)
        ++that.data.notifyServiceSearchIndex;
      wx.getBLEDeviceCharacteristics({
        // 这里的 devId 需要在 getBluetoothDevices 或 onBluetoothDeviceFound 接口中获取
        deviceId: that.data.devId,
        // 这里的 notifyServiceId 需要在 getBLEDeviceServices 接口中获取
        serviceId: that.data.notifyServiceId,
        complete: function () {
          console.log('device getBLEDeviceCharacteristics complete', that.data.notifyServiceSearchIndex)
          //递归调用自身直到找到notify特征或遍历完所有特征
          that.seekFirstNotifyCharacteristic()
        },
        success: function (res) {
          console.log('device getBLEDeviceCharacteristics:', res.characteristics)
          for (var n = 0; n < res.characteristics.length && that.data.notifyCharacteristicId == 'invalid'; ++n) {
            if (res.characteristics[n].properties.notify == true) {
              console.log('device notify Characteristics found', n)
              that.data.notifyCharacteristicId = res.characteristics[n].uuid
              console.log('notify servcie:', that.data.notifyServiceId)
              console.log('notify characteristic:', that.data.notifyCharacteristicId)
              console.log('start notify')
              wx.notifyBLECharacteristicValueChanged({
                state: true, // 启用 notify 功能
                // 这里的 devId 需要在 getBluetoothDevices 或 onBluetoothDeviceFound 接口中获取
                deviceId: that.data.devId,
                // 这里的 notifyServiceId 需要在 getBLEDeviceServices 接口中获取
                serviceId: that.data.notifyServiceId,
                // 这里的 notifyCharacteristicId 需要在 getBLEDeviceCharacteristics 接口中获取
                characteristicId: that.data.notifyCharacteristicId,
                success: function (res) {
                  console.log('notifyBLECharacteristicValueChanged success', res.errMsg)
                  wx.showToast({
                    title: '连接成功',
                    icon: 'success',
                    duration: 2000
                  })
                  that.setData({
                    currentConnectionState: '已连接'
                  });

                  // 寻找第一个write特征
                  that.data.writeServiceSearchIndex = 0
                  that.data.writeCharacteristicId = 'invalid'
                  that.seekFirstWriteCharacteristic()

                },
                fail: function (res) {
                  console.log(res)
                }
              })
            }
          }
        }
      })
    }
  },
  seekFirstWriteCharacteristic: function () {
    var that = this;
    if (that.data.writeServiceSearchIndex < that.data.services.length && that.data.writeCharacteristicId == 'invalid') {
      that.data.writeServiceId = that.data.services[that.data.writeServiceSearchIndex].uuid
      console.log('Search service index ', that.data.writeServiceSearchIndex, 'service: ', that.data.writeServiceId)
        ++that.data.writeServiceSearchIndex;
      wx.getBLEDeviceCharacteristics({
        // 这里的 devId 需要在 getBluetoothDevices 或 onBluetoothDeviceFound 接口中获取
        deviceId: that.data.devId,
        // 这里的 writeServiceId 需要在 getBLEDeviceServices 接口中获取
        serviceId: that.data.writeServiceId,
        complete: function () {
          console.log('device getBLEDeviceCharacteristics complete', that.data.writeServiceSearchIndex)
          //递归调用自身直到找到write特征或遍历完所有特征
          that.seekFirstWriteCharacteristic()
        },
        success: function (res) {
          console.log('device getBLEDeviceCharacteristics:', res.characteristics)
          for (var n = 0; n < res.characteristics.length && that.data.writeCharacteristicId == 'invalid'; ++n) {
            if (res.characteristics[n].properties.write == true) {
              console.log('device write Characteristics found', n)
              that.data.writeCharacteristicId = res.characteristics[n].uuid
              console.log('write servcie:', that.data.writeServiceId)
              console.log('write characteristic:', that.data.writeCharacteristicId)
            }
          }
        }
      })
    }
  },
  onLoad: function (options) {

    let json = wx.getStorageSync('bikeInfo')
    console.log(JSON.parse(json))
    this.setData({
      getBikeInfo: JSON.parse(json)
    })

    var that = this;
    that.setData({
      options: options
    });
    var btDevId = options.devId;
    var num = that.data.services.length
    that.data.services.splice(0, num)
    that.setData({
      services: that.data.services
    });
    /** wx.openBluetoothAdapter({
      success: function (res) {
        console.log(res)
      }
    }); **/
    for (var i = 0; i < app.globalData.btDevices.length; ++i) {
      if (app.globalData.btDevices[i].devId == btDevId) {
        console.log('!!!createBLEConnection', btDevId)
        that.setData({
          currentDevName: app.globalData.btDevices[i].name,
          currentConnectionState: '连接中...',
        });
        wx.createBLEConnection({
          // 这里的 btDevId 需要在上面的 getBluetoothDevices 或 onBluetoothDeviceFound 接口中获取
          deviceId: app.globalData.btDevices[i].devId,
          success: function (res) {
            that.data.devId = btDevId
            wx.getBLEDeviceServices({
              // 这里的 devId 需要在 getBluetoothDevices 或 onBluetoothDeviceFound 接口中获取
              deviceId: that.data.devId,
              success: function (res) {
                console.log('device services:', res.services)
                // 保存已连接设备的服务
                for (var j = 0; j < res.services.length; ++j) {
                  var newService = [
                    res.services[j],
                  ]
                  that.data.services = that.data.services.concat(newService)
                  console.log(j, that.data.services.length, res.services.length)
                }
                that.setData({
                  services: that.data.services
                });

                // 寻找第一个Notify特征
                that.data.notifyServiceSearchIndex = 0
                that.data.notifyCharacteristicId = 'invalid'
                that.seekFirstNotifyCharacteristic()
              },
              fail(res) {
                consoele.log("重新连接失败，失败原因：" + res)
              }
            })
          }
        })
        break;
      }
    }
    wx.onBLEConnectionStateChange(function (res) {
        console.log("连接状态改变" + res)
        if (res.connected) {

        } else {
          that.setData({
            currentConnectionState: "连接断开"
          });
          console.log("276:  " + that.data.unload);
          if (!that.data.unload) {
            wx.showModal({
              title: '断开连接',
              content: '未知原因与设备已连接断开',
              confirmText: '重新连接',
              cancelText: '返回',
              success: function (res) {
                if (res.confirm) {
                  wx.createBLEConnection({
                    // 这里的 deviceId 需要已经通过 createBLEConnection 与对应设备建立链接
                    deviceId: that.data.options.devId,
                    success(res) {
                      wx.getBLEDeviceServices({
                        // 这里的 devId 需要在 getBluetoothDevices 或 onBluetoothDeviceFound 接口中获取
                        deviceId: that.data.devId,
                        success: function (res) {
                          console.log('device services:', res.services)
                          // 保存已连接设备的服务
                          for (var j = 0; j < res.services.length; ++j) {
                            var newService = [
                              res.services[j],
                            ]
                            that.data.services = that.data.services.concat(newService)
                            console.log(j, that.data.services.length, res.services.length)
                          }
                          that.setData({
                            services: that.data.services
                          });

                          // 寻找第一个Notify特征
                          that.data.notifyServiceSearchIndex = 0
                          that.data.notifyCharacteristicId = 'invalid'
                          that.seekFirstNotifyCharacteristic()
                        },
                        fail(res) {
                          consoele.log("重新连接失败，失败原因：" + res)
                        }
                      })
                      console.log("重新连接")
                      console.log(res)
                      /** wx.showToast({
                        title: '连接成功123',
                        icon: 'success',
                        duration: 2000
                      })
                      that.setData({
                        currentConnectionState: '已连接'
                      }); **/
                    },
                    fail(res) {
                      console.log("重新连接失败")
                      console.log(res);
                    }
                  })
                } else if (res.cancel) {
                  wx.navigateBack(1);
                }
              }
            })
          }
        }
        console.log("256: " + res.connected)
        that.data.connected = res.connected;
        console.log("connected: " + res.connected);
        console.log(`device ${res.deviceId} state has changed, connected: ${res.connected}`)
      }),
      wx.onBluetoothAdapterStateChange(function (res) {
        console.log(`adapterState changed, now is`, res)
      })
  },
  ab2hex: function (buffer) {
    var str = "";
    let hexArr = Array.prototype.map.call(
      new Uint8Array(buffer),
      function (bit) {
        return ('00' + bit.toString(16)).slice(-2);
      }
    )
    // console.log("210:  " + hexArr.join('') + " " + hexArr.length + "    " + hexArr[0] + "    " + hexArr[1]);
    for (var i = 0; i < hexArr.length - 1; i++) {
      str += hexArr[i] + " ";
    }
    str += hexArr[hexArr.length - 1];
    return str;
    // return hexArr.join('');
  },
  onShow: function () {
    var that = this;
    // that.onLoad(that.data.options);
    wx.onBLECharacteristicValueChange(function (res) {
      var recv_data = that.data.recv_data;
      var recv_data_hex = that.data.recv_data_hex;
      var recv = that.data.recv;
      var arr;
      var str = '';
      var val = '';
      // console.log(`characteristic ${res.characteristicId} has changed, now is ${res.value}`)
      if (that.data.hex) {
        str += that.ab2hex(res.value)
        recv_data_hex += str;
        recv += that.ab2str(res.value);
        if (!recv_data == '') {
          recv_data += " " + str;
        } else {
          recv_data += str;
        }
        that.setData({
          recvTotalBytes: recv.length.toString(),
          recv_data: recv_data,
          recv: recv,
          recv_data_hex: recv_data_hex,
          scrollTop: that.data.scrollTop + 20
        });
      } else {
        str = String.fromCharCode.apply(null, new Uint8Array(res.value)).replace("0D0A", "0A");
        recv_data += str.replace("0A", "\n");
        arr = str.split("0A");
        if (!recv_data_hex == '') {
          recv_data_hex += " " + that.ab2hex(res.value);
        } else {
          recv_data_hex += that.ab2hex(res.value);
        }
        that.setData({
          recvTotalBytes: recv_data.length.toString(),
          recv_data: recv_data,
          recv_data_hex: recv_data_hex,
          scrollTop: that.data.scrollTop + 20
        });
      }
    })

  },

  onUnload: function () {
    var that = this;
    that.setData({
      unload: true,
      currentConnectionState: '断开连接'
    });
    that.data.detail = false
    wx.closeBLEConnection({
      deviceId: that.data.devId,
      success: function (res) {
        console.log(res)
      }
    })

    wx.closeBluetoothAdapter({
      success(res) {
        console.log(res)
      }
    })

  },

  encode_utf8: function (s) {
    return unescape(encodeURIComponent(s));
  },

  decode_utf8: function (s) {
    return decodeURIComponent(escape(s));
  },

  ab2str: function (buf) {
    var that = this;
    var s = String.fromCharCode.apply(null, new Uint8Array(buf));
    return that.decode_utf8(that.decode_utf8(s))
  },
  str2ab: function (str) {
    var that = this;
    // var s = that.encode_utf8(str)
    const s = str;
    var buf = new ArrayBuffer(s.length);
    var bufView = new Uint8Array(buf);
    console.log(bufView)
    for (var i = 0, strLen = s.length; i < strLen; i++) {
      bufView[i] = s.charCodeAt(i);
    }
    return buf;
  },
  // 发送按钮点击事件
  bingButtonSendData: function () {
    // var that = this;
    // var data;
    // if (this.data.hex) {
    //   var val = '';
    //   var arr = this.data.send_data.split(" ");
    //   for (var i = 0; i < arr.length; i++) {
    //     val += String.fromCharCode(parseInt(arr[i], 16));
    //   };
    //   data = val;
    // } else {
    //   data = that.data.send_data;
    // };
    // if (data == "") {
    //   return;
    // }
    // that.data.sendTotalBytesCounter += data.length;
    // that.setData({
    //   sendTotalBytes: that.data.sendTotalBytesCounter.toString(),
    // });
    // if (data.length > 100) {

    // }
    let date = new Date();
    let year = date.getFullYear(); //获取完整的年份(4位)
    let month = date.getMonth(); //获取当前月份(0-11,0代表1月)
    let day = date.getDate(); //获取当前日(1-31)
    let datestart = `${year}年${month+1}月${day}`
    let dateEnd = `${year+3}年${month+1}月${day}`
    let str = `
    SIZE 55 mm,35 mm
    SET GAP 2mm
    CLS

    TEXT 20,10,"TSS24.BF2",0,1,1,"姓名：${this.data.getBikeInfo.userName}"
    TEXT 20,50,"TSS24.BF2",0,1,1,"身份证号：${this.data.getBikeInfo.idCard}"
    TEXT 20,90,"TSS24.BF2",0,1,1,"厂牌车型：${this.data.getBikeInfo.brandName}"
    TEXT 20,130,"TSS24.BF2",0,1,1,"车牌号：${this.data.getBikeInfo.licensePlate}"
    TEXT 20,170,"TSS24.BF2",0,1,1,"车架号：${this.data.getBikeInfo.vin}"
    TEXT 20,210,"TSS24.BF2",0,1,1,"发动机号：${this.data.getBikeInfo.motornumber}"
    TEXT 20,250,"TSS24.BF2",0,1,1,"保险期限：${this.data.getBikeInfo.time}"
    PRINT 1,1`

    // let str = `
    // SIZE 55 mm,35 mm
    // SET GAP 2mm
    // CLS
    // QRCODE 200,200,"L",4,90,"123456789"
    // PRINT 1,1
    // `
    this.sentDirectly(str);
  },
  sentDirectly: function (data) {
    var that = this;
    // 向蓝牙设备发送数据
    var uint8Buf = gbks.hexStringToBuff(data)
    // var uf = that.str2ab(data)
    //console.log("发送的二进制数据： " + data.charCodeAt().toString(2));
    console.log(that.data.devId)
    console.log(that.data.writeServiceId)
    console.log(that.data.writeCharacteristicId)
    console.log("===========********=========")
    console.log(data)
    console.log("====================")

    console.log(uint8Buf)

    console.log("=========************===========")

    wx.writeBLECharacteristicValue({
      // 这里的 devId 需要在 getBluetoothDevices 或 onBluetoothDeviceFound 接口中获取
      deviceId: that.data.devId,
      // 这里的 serviceId 需要在 getBLEDeviceServices 接口中获取
      serviceId: that.data.writeServiceId,
      // 这里的 writeCharacteristicId 需要在上面的 getBLEDeviceCharacteristics 接口中获取
      characteristicId: that.data.writeCharacteristicId,
      // 这里的value是ArrayBuffer类型
      value: uint8Buf,
      success: function (res) {
        console.log('writeBLECharacteristicValue success', res.errMsg)
      },
      fail(res) {
        console.log("发送数据失败", res)
      }
    })
  },
  // 文件定时发送
  cycleToSend: function () {
    var that = this;
    var flag = 0;
    var time = that.data.scrollTop;
    var j = setInterval(function () {
      if (flag >= that.data.txd.length) {
        clearInterval(j);
        flag = 0;
        return;
      }
      that.data.sendTotalBytesCounter += that.data.txd[flag].length
      that.setData({
        sendTotalBytes: that.data.sendTotalBytesCounter.toString(),
      });
      // 向蓝牙设备发送数据
      var uint8Buf = that.str2ab(that.data.txd[flag])
      let buffer = new ArrayBuffer(that.data.txd[flag].length)
      let dataView = new DataView(buffer)
      for (var i = 0; i < that.data.txd[flag].length; ++i) {
        dataView.setUint8(i, uint8Buf[i])
      }
      wx.writeBLECharacteristicValue({
        // 这里的 devId 需要在 getBluetoothDevices 或 onBluetoothDeviceFound 接口中获取
        deviceId: that.data.devId,
        // 这里的 serviceId 需要在 getBLEDeviceServices 接口中获取
        serviceId: that.data.writeServiceId,
        // 这里的 writeCharacteristicId 需要在上面的 getBLEDeviceCharacteristics 接口中获取
        characteristicId: that.data.writeCharacteristicId,
        // 这里的value是ArrayBuffer类型
        value: buffer,
        success: function (res) {
          that.setData({
            progress: parseInt((flag / that.data.txd.length) * 100)
          });
        }
      })
      flag++;
    }, time);
  },
  periodicSendSwitchChange: function (e) {
    var that = this;
    var data;
    var length;
    console.log(that.data.time)
    that.setData({
      detail: e.detail.value
    });
    if (that.data.send_data == '') {
      return;
    }
    if (that.data.hex) {
      var val = "";
      var arr = this.data.send_data.split(" ");
      for (var i = 0; i < arr.length; i++) {
        val += String.fromCharCode(parseInt(arr[i], 16));
      };
      data = val;
      length = data.length;
    } else {

      data = that.data.send_data;
      length = data.length;
    };
    that.data.txd[0] = data;
    console.log(that.data.time)
    var j = setInterval(function () {
      if (!that.data.detail) {
        clearInterval(j);
        return;
      }
      if (that.data.currentConnectionState == "已连接") {
        that.data.sendTotalBytesCounter += length
        that.setData({
          sendTotalBytes: that.data.sendTotalBytesCounter.toString(),
        });
        that.sentDirectly(that.data.txd[0]);
      }
    }, that.data.time);

  },
  file: function () {
    var that = this;
    that.setData({
      size: ''
    });
    wx.chooseImage({
      count: 1,
      success(res) {
        let fs = wx.getFileSystemManager()
        fs.readFile({
          filePath: res.tempFilePaths[0],
          encoding: 'binary',
          success(res) {
            that.subpackage(res, 100);
            // that.cycleToSend();

          },
          fail(res) {
            console.log("fail:" + res.data);
          }
        })
      }
    });
    // that.data.detail = false;
  },
  // 对文件进行切分
  subpackage: function (res, num) {
    var that = this;
    var length = res.data.length;
    that.setData({
      size: "文件大小：" + res.data.length + "字节"
    });
    var time = parseInt(length / num);
    var remainder = length % num;
    var start = 0;
    var stop = 0;
    if (remainder != 0) {
      time = time + 1;
    }
    for (var i = 0; i < time; i++) {
      if (i != 0) {
        start = num * i;
        if (time - i == 1) {
          stop = start + (length - start);
        } else {
          stop = start + num;
        }
      } else {
        start = num * i;
        stop = start + num;
      }
      that.data.txd.push(res.data.substring(start, stop));
    }
  },
  HexSwitchChange: function (e) {
    var that = this;
    if (e.detail.value) {
      var val = "";
      if (this.data.send_data != '') {
        for (var i = 0; i < this.data.send_data.length; i++) {
          if (val == "")
            val = this.data.send_data.charCodeAt(i).toString(16);
          else {
            val += " " + this.data.send_data.charCodeAt(i).toString(16);
          }
        }
        this.setData({
          send_data: val
        })
      }
      this.setData({
        hex: e.detail.value,
        recv: that.data.recv_data,
        recv_data: that.data.recv_data_hex
      })
    } else {
      var val = "";
      if (this.data.send_data != '') {
        var arr = this.data.send_data.split(" ");
        for (var i = 0; i < arr.length; i++) {
          val += String.fromCharCode(parseInt(arr[i], 16));
        };
        this.setData({
          send_data: val
        })
      }
      this.setData({
        hex: e.detail.value,
        recv_data_hex: that.data.recv_data,
        recv_data: that.data.recv
      })
    }
  },

  bindButtonClearAll: function (e) {
    var that = this;
    that.setData({
      recv_data: '',
      recv_data_hex: '',
      recv: '',
      recvTotalBytes: '0',
      recvCrc32: '0',
      sendTotalBytesCounter: 0,
      sendTotalBytes: '0',
      sendCrc32: '0',
    });
  },
  bindTextAreaBlur: function (e) {
    var that = this;
    that.setData({
      send_data: e.detail.value
    })
  },


  bindKeyInput: function (e) {
    var that = this;
    if (e.detail.value == "") {
      that.setData({
        time: 1000
      })
    } else {
      that.setData({
        time: e.detail.value
      })
    }
  },
});