// pages/erectCar/erectCar.js
import {
  subscriberMembers,
  bikeSearch
} from "../../request/getIndexData"
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userInfo: {},
    bikeList: []
  },
  getSubscriberMembers(par) {
    subscriberMembers(par).then(res => {
      console.log(res)
      this.setData({
        userInfo: res.data.results[0]
      })
    })
  },
  getbikeSearch(par) {
    wx.showLoading({
      title: '正在加载数据',
      mask:true
    })
    bikeSearch(par).then(res => {
      wx.hideLoading()
      this.setData({
        bikeList: res.data.results
      })
    })
  },
  addCarInfosingle(e){
    wx.setStorageSync('userUrn', e.currentTarget.dataset.urn)
    wx.setStorageSync('ownerName', e.currentTarget.dataset.name)
    wx.navigateTo({
      url: '/pages/insatllCarInfo/insatllCarInfo',
    })
  },
  addCarInfo(e) {
    if (e.currentTarget.dataset.user.idCards.length) {
      wx.setStorageSync('userUrn', e.currentTarget.dataset.user.urn)
      wx.setStorageSync('ownerName', e.currentTarget.dataset.name)
      wx.navigateTo({
        url: '/pages/insatllCarInfo/insatllCarInfo',
      })
    }else{
      wx.showToast({
        title: '用户信息不全,请先补全用户信息',
        icon: 'none',
        duration: 2000
      })
    }


  },
  editUser(e) {
    console.log(e)
    let phone = e.currentTarget.dataset.phone
    wx.navigateTo({
      url: '/pages/editUser/editUser?phone=' + phone,
    })
  },
  editCar(e) {
    console.log(e.currentTarget.dataset);
    wx.setStorageSync('ownerName', e.currentTarget.dataset.name)
    let urn = e.currentTarget.dataset.urn
    wx.navigateTo({
      url: '/pages/editCar/editCar?urn=' + urn
    })
  },
  printCard(e) {
    console.log(e)
    let json = {
      brandName: e.currentTarget.dataset.brandname,
      userName: this.data.userInfo.member.nickName,
      idCard: this.data.userInfo.idCards[0],
      licensePlate: e.currentTarget.dataset.licenseplate,
      motornumber: e.currentTarget.dataset.motornumber,
      vin: e.currentTarget.dataset.vin,
      time:`${e.currentTarget.dataset.start}-${e.currentTarget.dataset.expired}`
    }
    wx.setStorageSync('bikeInfo', JSON.stringify(json))
    wx.navigateTo({
      url: '/pages/bluetooth/bluetooth',
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if (options.phone) {
      let par = {
        phone: options.phone
      }
      this.getSubscriberMembers(par)
    }

    let param = {
      owner: options.userUrn
    }
    this.getbikeSearch(param)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})