

// pages/carownerinfo/carownerinfo.js
import {
  searchDistrict,
  subscriberMembers,
  subscriberEntities
} from "../../request/getIndexData"
Page({

  /**
   * 页面的初始数据
   */
  data: {
    getcarInfo:{},
    carInfo:{
      name:'',
      idCard:'',
      gender:'',
      legalAddress:'',
      phone:'',
      mailAddress:'',
      contacts:[
        {
          person:'',
          mobile:''
        }
      ],
      member:{
        nickName:''
      }
    },
    zhenarray: [],
    zhenindex: 0,
  },
  bindPickerChangezhen: function (e) {
    console.log('picker发送选择改变，携带值为', e)
    let index = e.detail.value
    this.setData({
      zhenindex: index
    })
    this.data.getcarInfo.districtUrn = this.data.zhenarray[index].urn

  },
  getsearchDistrict() {
    let par = {
      parents: [
        "urn:bh:district:1"
      ],
      start:0,
      count:50
    }
    searchDistrict(par).then(res => {
      res.data.results.unshift({
        name: "-请选择-"
      })
      this.setData({
        zhenarray: res.data.results
      })
    })
  },
  userName(e){
    console.log(e)
    this.data.getcarInfo.name = e.detail.value
    this.data.getcarInfo.member.nickName = e.detail.value
  },
  userIcCard(e){
    this.data.getcarInfo.idCards =[ e.detail.value]
  },
  unblurIdcard(){
    if(this.data.getcarInfo.idCards[0].length==18){
      this.data.getcarInfo.idCards;
    }else{
      wx.showToast({
        title: '身份证号为18位',
        icon: 'none',
        duration: 2000
      })
    }
  },
  userSex(e){
    console.log(e)
    this.data.getcarInfo.member.gender = e.detail.value
  },
  userLegalAddress(e){
    this.data.getcarInfo.legalAddress.consolidated = e.detail.value
  },
  userPhone(e){
    this.data.getcarInfo.member.phones = [e.detail.value]
  },
  userMailAddress(e){
    this.data.getcarInfo.mailAddress.consolidated = e.detail.value
  },
  jinjisuer(e){
    this.data.getcarInfo.emergencyContacts[0].person = e.detail.value
  },
  jinjisuerphone(e){
    this.data.getcarInfo.emergencyContacts[0].mobile = e.detail.value
  },
  submituserInfo(){
    // this.data.carInfo.districtUrn = wx.getStorageSync('districtId')
    if(!this.data.getcarInfo.idCards.length){
      wx.showToast({
        title: '请填写身份证号',
        icon: 'none',
        duration: 2000
      })
      return  false;
    }
    if(this.data.getcarInfo.idCards[0].length==18){
      this.data.getcarInfo.idCards;
    }else{
      wx.showToast({
        title: '身份证号为18位',
        icon: 'none',
        duration: 2000
      })
      return false;
    }
    subscriberEntities([this.data.getcarInfo]).then(res=>{
      if(res.data.error){
        console.info(res.data.error)
      }else{
        wx.showToast({
          title: '更新用户信息成功',
          icon: 'none',
          duration: 2000
        })
        setTimeout(()=>{
          wx.reLaunch({
            url:'/pages/erectCar/erectCar?userUrn=' + res.data.results[0].urn + '&phone=' + res.data.results[0].member.phones[0],
          })
        },1000)
        
      }
    })
  },
  getSubscriberMembers(par){
    subscriberMembers(par).then(res => {
      console.log(res)
      this.setData({
        getcarInfo:res.data.results[0]
      })
      this.data.getcarInfo.emergencyContacts = [{
        person:'',
        mobile:''
      }]
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options)
    let par = {
      phone: options.phone
    }
    this.getSubscriberMembers(par)
    // wx.navigateTo({
    //   url: '/pages/insatllCarInfo/insatllCarInfo',
    // })
    this.getsearchDistrict()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})