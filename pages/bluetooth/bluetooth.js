var app = getApp();
Page({
	data: {
		filterId: 1,
		btDevices: app.globalData.btDevices
	},
	onLoad: function () {
		var self = this;
		var that = this
		for (var i = 0; i < that.data.btDevices.length; ++i) {
			that.data.btDevices.pop()
		}
		that.setData({
			btDevices : that.data.btDevices
		});

		var launchToastDuration;
		if(app.globalData.platform == 'ios')
		{
			launchToastDuration = 1500
		}
		else
		{
			launchToastDuration = 2500
		}
		wx.showToast({
		title: '猎熊座蓝牙',
		icon: 'loading',
		mask: true,
		image: '/img/fsc/fangyi.png',
		duration: launchToastDuration
		})
	},

	onHide: function () {
		var that = this
		wx.stopBluetoothDevicesDiscovery({
		success: function (res) {
			// console.log(res)
		}
		})
	},

	onShow: function () {
		var that = this

		if(app.globalData.platform == 'ios')
		{
			// console.log('ios bt')
		}
		else
		{
			// console.log('android bt')
		}

    wx.openBluetoothAdapter({
      success: function (res) {
        // console.log(res)
        wx.startBluetoothDevicesDiscovery({
          success: function (res) {
            // console.log(res)
          }
        })
      }
    })
    
		wx.onBluetoothDeviceFound(function(devices) {
			wx.getBluetoothDevices({
				success: function (res) {
				var ln = 0;
				// console.log(res,that.data.btDevices.length)
				if(that.data.btDevices.length != null)
					ln = that.data.btDevices.length
				for (var i = ln; i < res.devices.length; ++i)
				{
					//console.log(ln,that.data.btDevices.length,res.devices.length)
					var rssi_level_img;
					if(res.devices[i].RSSI > 0 || res.devices[i].name == '未知设备')
					{
						continue;
					}
					if(res.devices[i].RSSI > -40)
					{
						rssi_level_img = '/img/scan/5.png'
					}
					else if(res.devices[i].RSSI > -50)
					{
						rssi_level_img = '/img/scan/4.png'
					}
					else if(res.devices[i].RSSI > -60)
					{
						rssi_level_img = '/img/scan/3.png'
					}
					else if(res.devices[i].RSSI > -70)
					{
						rssi_level_img = '/img/scan/2.png'
					}
					else
					{
						rssi_level_img = '/img/scan/1.png'
					}
					var newBtDevice = [{
						rssi : res.devices[i].RSSI,
						name : res.devices[i].name,
						devId : res.devices[i].deviceId,
						img : rssi_level_img,
					}];

					// dump redundant device
					for (var k = 0; k < that.data.btDevices.length; ++k)
					{
						//console.log('new ',res.devices[i].deviceId,'old',that.data.btDevices[k].devId)
						if(res.devices[i].deviceId == that.data.btDevices[k].devId)
						{
							//console.log('dump',k,that.data.btDevices[k].devId)
							that.data.btDevices.splice(k,1);
							break;
						}
					}
					that.data.btDevices = that.data.btDevices.concat(newBtDevice)
				}
				that.setData({
					btDevices : that.data.btDevices
				});

				app.globalData.btDevices = that.data.btDevices
				}
			})

		})

	},

	onPullDownRefresh: function() {
		// 下拉清空记录，并重新搜索
		// console.log('onPullDownRefresh')
		var that = this
		wx.stopPullDownRefresh()
		wx.stopBluetoothDevicesDiscovery({
		success: function (res) {
			// console.log(res)
			wx.closeBluetoothAdapter({
			success: function (res) {
				// console.log(res)
				var num = that.data.btDevices.length
				that.data.btDevices.splice(0,num)
				that.setData({
					btDevices : that.data.btDevices
				});			
				wx.openBluetoothAdapter({
				success: function (res) {
					// console.log(res)
					wx.startBluetoothDevicesDiscovery({
					//services: ['FFF0'],
					success: function (res) {
						// console.log(res)
					}
					})
				}
				})
			}
			})
		}
		})
	},

	onScroll: function (e) {
		if (e.detail.scrollTop > 100 && !this.data.scrollDown) {
			this.setData({
				scrollDown: true
			});
		} else if (e.detail.scrollTop < 100 && this.data.scrollDown) {
			this.setData({
				scrollDown: false
			});
		}
	},
	tapFilter: function (e) {
		switch (e.target.dataset.id) {	
			case '2':
				this.data.btDevices.sort(function (a, b) {
					return b.rssi - a.rssi;
				});
				break;
		}
		this.setData({
			filterId: e.target.dataset.id,
			btDevices: this.data.btDevices
		});
	}
});

