// pages/carInfo/carInfo.js
import {
  bikeEntities,
  getopenId,
  login
} from "../../request/getIndexData"
Page({

  /**
   * 页面的初始数据
   */
  data: {
    carbodyce: '',
    params: {}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  chooseImg(e) {
    let type = e.currentTarget.dataset.type
    let that = this
    wx.chooseImage({
      count: 1,
      sizeType: ['original', 'compressed'],
      sourceType: ['camera'],
      success(res) {
        wx.showLoading({
          title: '图片上传中',
          mask: true
        })
        console.log(that.data.params)
        let tempFilePaths = res.tempFilePaths
        that.data.params.snapshots2[type] = [tempFilePaths[0]]
        // tempFilePath可以作为img标签的src属性显示图片
        that.setData({
          params: that.data.params
        })
        let urn = `urn:bh:bike:${that.data.params.licensePlate}:${that.data.params.vin}`
        wx.uploadFile({
          filePath: tempFilePaths[0],
          name: 'file',
          // url: `http://47.100.19.229:9001/public/api/sphyrnidae/v1/bike/image/upload?urn=` + urn,
          url: `https://radar-backend.bearhunting.cn/sphyrnidae/v1/bike/image/upload?urn=` + urn,
          // url: `http://192.168.63.161:9001/public/api/sphyrnidae/v1/bike/image/upload?urn=` + urn,
          header: {
            "Content-Type": "multipart/form-data",
          },
          success: function (res) {
            wx.hideLoading({
              title: '图片上传成功',
            })
            that.data.params.snapshots[type] = [{
              url: JSON.parse(res.data).result
            }]
            that.setData({
              params: that.data.params
            })
          },
          fail: function () {
            that.data.params.snapshots2[type] = []
            wx.showToast({
              title: '图片上传失败',
              icon: 'error',
              duration: 2000
            })
          }
        })
      }
    })
  },
  closeCurrent(e) {
    let type = e.currentTarget.dataset.type
    this.data.params.snapshots[type] = []
    this.data.params.snapshots2[type] = []
    this.setData({
      params: this.data.params
    })
  },
  submitALL() {
    if(this.data.params.snapshots.FRONT.length==0){
      wx.showToast({
        title: '请拍车头照片',
        icon: 'none',
        duration: 2000
      })
      return false;
    }
    if(this.data.params.snapshots.LEFT.length==0){
      wx.showToast({
        title: '请拍车辆侧身照片',
        icon: 'none',
        duration: 2000
      })
      return false;
    }
    if(this.data.params.snapshots.LICENSE.length==0){
      wx.showToast({
        title: '请拍车辆牌照照片',
        icon: 'none',
        duration: 2000
      })
      return false;
    }
    if(this.data.params.snapshots.BACK.length==0){
      wx.showToast({
        title: '请拍车尾照片',
        icon: 'none',
        duration: 2000
      })
      return false;
    }
    if(this.data.params.snapshots.SIN_FRONT.length==0){
      wx.showToast({
        title: '请拍车主身份证正面照片',
        icon: 'none',
        duration: 2000
      })
      return false;
    }
    if(this.data.params.snapshots.SIN_BACK.length==0){
      wx.showToast({
        title: '请拍车主身份证反面照片',
        icon: 'none',
        duration: 2000
      })
      return false;
    }
    if(this.data.params.snapshots.ENGINE.length==0){
      wx.showToast({
        title: '请拍电机号照片',
        icon: 'none',
        duration: 2000
      })
      return false;
    }
    if(this.data.params.snapshots.VIN.length==0){
      wx.showToast({
        title: '请拍车架号照片',
        icon: 'none',
        duration: 2000
      })
      return false;
    }
    this.data.params.zones = wx.getStorageSync('districtId')
    this.data.params.properties = {
      locationName: wx.getStorageSync('locationName')
    }
    this.data.params.ownerUrn = wx.getStorageSync('userUrn')
    this.data.params.ownerName = wx.getStorageSync('ownerName')
    // this.data.params = {
    //   "vin": "123456",
    //   "motorNumber": "123456",
    //   "colour": ["CYAN"],
    //   "productModel": {
    //     "brandName": "123456"
    //   },
    //   "licensePlate": "123456",
    //   "ownerUrn": "urn:cn:member:phone:123456",
    //   "rfIdList": [{
    //     "urn": "urn:bh:rfid:0093743884",
    //     "imageUrl": "rfid/20210602134543wpp2sfVWtmp_d5b307a12027ec61451d53afe85f09a1503373a6b6c48e66.jpg",
    //     "rfTagLocation": "其他位置",
    //     "idx": "10",
    //     "img": "wxfile://tmp_d5b307a12027ec61451d53afe85f09a1503373a6b6c48e66.jpg"
    //   }],
    //   "snapshots": {
    //     "LEFT": [{
    //       "url": "bike/20210602134557pUh8Xns2tmp_4829f42326a4dab7d52d1a5436365cdb4b63377ada72f4a4.jpg"
    //     }],
    //     "RIGHT": [],
    //     "FRONT": [],
    //     "BACK": [],
    //     "BRAND": [],
    //     "LICENSE": [],
    //     "SIN_FRONT": [],
    //     "SIN_BACK": [],
    //     "ENGINE": [],
    //     "VIN": []
    //   },
    //   "snapshots2": {
    //     "LEFT": ["wxfile://tmp_4829f42326a4dab7d52d1a5436365cdb4b63377ada72f4a4.jpg"],
    //     "RIGHT": [],
    //     "FRONT": [],
    //     "BACK": [],
    //     "BRAND": [],
    //     "LICENSE": [],
    //     "SIN_FRONT": [],
    //     "SIN_BACK": [],
    //     "ENGINE": [],
    //     "VIN": []
    //   },
    //   "zones": "urn:bh:district:169"
    // }

    bikeEntities([this.data.params]).then(res => {
      console.log(res)
      if (res.statusCode == 400) {
        wx.showModal({
          title: '服务停止啦',
          content: res.data,
          showCancel: false,
        })
      } else {
        if (res.data.error) {
          if (res.data.error.code == 401) {
            let that = this
            wx.login({
              success(res) {
                if (res.code) {
                  let params = {
                    code: res.code,
                  }
                  getopenId(params).then(res => {
                    let par = {
                      openId: res.data.result.openid,
                    }
                    login(par).then(res => {
                      if (res.data.result.auth) {
                        let cookie = res.data.result.auth.token
                        wx.setStorageSync("cookieInfo", cookie)
                        wx.showToast({
                          title: '请重新提交',
                          icon: 'none',
                          duration: 2000
                        })
                      } else {
                        wx.showToast({
                          title: '登录信息失效请退出重进',
                          icon: 'none',
                          duration: 2000
                        })
                      }
                    })
                  })

                } else {
                  wx.showToast({
                    title: '登录信息失效请退出重进',
                    icon: 'none',
                    duration: 2000
                  })
                }
              }
            })
          }
        } else {
          wx.showToast({
            title: '提交成功',
            icon: 'none',
            duration: 2000
          })
          setTimeout(() => {
            wx.switchTab({
              url: '/pages/homepage/homepage',
            })
          }, 1000)
        }
      }

    })
  },
  // message: "JSON parse error: Cannot deserialize instance of `cn.bearhunting.common.coral.entities.real.objects.BikeEntity` out of START_ARRAY token; nested exception is com.fasterxml.jackson.databind.exc.MismatchedInputException: Cannot deserialize instance of `cn.bearhunting.common.coral.entities.real.objects.BikeEntity` out of START_ARRAY token↵ at [Source: (PushbackInputStream); line: 1, column: 2] (through reference chain: cn.bearhunting.common.core.models.DataCollection[0])"

  onLoad: function (options) {
    let params = wx.getStorageSync('eleBikeInfo')
    this.setData({
      params: JSON.parse(params)
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function (e) {
    // console.log(e)
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})