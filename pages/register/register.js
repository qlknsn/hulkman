// pages/register/register.js
import {
  wxBind
} from "../../request/getIndexData"
let app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    name: '',
    phone: ''
  },
  nameInput(e) {
    console.log(e.detail.value)
    this.setData({
      name: e.detail.value
    })
  },
  phoneInput(e) {
    console.log(e.detail.value)
    this.setData({
      phone: e.detail.value
    })
  },
  bindWxuser() {
    // console.log(app.globalData.sesszionKey)
    if(!this.data.name){
      wx.showToast({
        title: '请填写姓名',
        icon: 'none',
        duration: 2000
      })
      return  false;
    }
    if(!this.data.phone){
      wx.showToast({
        title: '请填写手机号',
        icon: 'none',
        duration: 2000
      })
      return  false;
    }
    let that = this
    
    wx.getUserProfile({
      desc: '用于完善会员资料',
      success: function (res_user) {
        console.log(res_user)
        let params = {
          encryptedData: res_user.encryptedData,
          iv: res_user.iv,
          signature: res_user.signature,
          name: that.data.name,
          phone: that.data.phone,
          openId: app.globalData.openId,
          sessionKey: app.globalData.sesszionKey,
          bindType: "ma",
          roles: ["BikeManager"]
        }
        wxBind(params).then(res => {
          console.log(res)
          if (res.data.error) {
            console.error('接口報錯')
            wx.showToast({
              title: res.data.error.message,
              icon: 'none',
              duration: 2000
            })
          } else {
            let cookie = res.data.result.auth.token
            console.log(cookie)
            wx.setStorageSync("cookieInfo", cookie)
            wx.reLaunch({
              url: '/pages/homepage/homepage',
            })
          }
        })
      },
      fail: function (err) {
        console.log(err)
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    options;
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})