import reqeust from './request'
export function getopenId(params) {
  return reqeust({
    url: '/sphyrnidae/v1/wechat/openid/ma',
    data: params,
    method: 'get'
  })
}
export function login(params) {
  return reqeust({
    url: '/sphyrnidae/v1/subscriber/login',
    method: 'post',
    data: params
  })
}
export function wxBind(params) {
  return reqeust({
    url: '/sphyrnidae/v1/subscriber/wxBind',
    method: 'post',
    data: params
  })
}
export function subscriberMember(params) {
  return reqeust({
    url: '/sphyrnidae/v1/subscriber/member',
    method: 'post',
    data: params
  })
}
// 获取所有颜色列表
export function simpleColours(params) {
  return reqeust({
    url: '/sphyrnidae/v1/bike/simpleColours',
    params: params
  })
}
// 获取所有颜色列表
export function rfTagLocations(params) {
  return reqeust({
    url: '/sphyrnidae/v1/rfid/rfTagLocations',
    params: params
  })
}
// 上传照片
export function uploadImg(params) {
  return reqeust({
    url: '/sphyrnidae/v1/bike/image/upload',
    method: 'post',
    data: params
  })
}
// 获取区域
export function searchDistrict(params) {
  if (params.count) {
    return reqeust({
      url: `/sphyrnidae/v1/district/search?start=${params.start}&count=${params.count}`,
      method: 'post',
      data: params
    })
  } else {
    return reqeust({
      url: `/sphyrnidae/v1/district/search`,
      method: 'post',
      data: params
    })
  }

}
// 添加电动车
export function bikeEntities(params) {
  let header = {
    'token': wx.getStorageSync("cookieInfo")
  }
  console.log(header)
  return reqeust({
    url: '/sphyrnidae/v1/bike/entities?action=create',
    method: 'post',
    header: header,
    data: params
  })
}
// 添加电动车
export function editBikeEntities(params) {
  let header = {
    'token': wx.getStorageSync("cookieInfo")
  }
  console.log(header)
  return reqeust({
    url: '/sphyrnidae/v1/bike/entities?action=update',
    method: 'post',
    header: header,
    data: params
  })
}
// 根据身份证查找用户
export function subscriberMembers(params) {
  return reqeust({
    url: '/sphyrnidae/v1/subscriber/members',
    method: 'post',
    data: params
  })
}
// 获取电动车信息
export function bikeSearch(params) {
  return reqeust({
    url: '/sphyrnidae/v1/bike/search',
    method: 'post',
    data: params
  })
}
// 修改车主信息
export function subscriberEntities(params) {
  return reqeust({
    url: '/sphyrnidae/v1/subscriber/entities',
    method: 'post',
    data: params
  })
}
//获取单个电动车数据
export function singlebikeEntity(params) {
  return reqeust({
    url: '/sphyrnidae/v1/bike/entity/' + params.urn,
  })
}
//获取个人中心
export function subscriberMine(params) {
  let header = {
    'token': wx.getStorageSync("cookieInfo")
  }
  return reqeust({
    url: '/sphyrnidae/v1/subscriber/mine',
    params: params,
    header: header,
  })
}
// 查询是否存在此标签
export function rfidEntity(params) {
  return reqeust({
    url: '/sphyrnidae/v1/rfid/entity/' + params.key,
  })
}
export function handleTask(params) {
  return reqeust({
    url: '/seaweed/v1/order/handle',
    method: 'post',
    data: params
  })
}
export function taskdetail(params) {
  return reqeust({
    url: '/seaweed/v1/order/detail?id=' + params.id,
  })
}
export function configList() {
  return reqeust({
    url: '/seaweed/v1/config/list'
  })
}