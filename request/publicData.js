const baseUrl= 'https://radar.bearhunting.cn';
// const baseUrl= 'http://192.168.63.131:8888/public/api';
// const baseUrl= 'http://47.100.19.229:9001';
const IMG_HOST = 'https://bh-radar.oss-cn-shanghai.aliyuncs.com/'
export {
  baseUrl,
  IMG_HOST
}