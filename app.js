// app.js
import {
  getopenId,
  login
} from "/request/getIndexData"
App({
  onLaunch() {
    // 展示本地存储能力
    const logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs)

    // 登录
    
    var self = this
    wx.getSystemInfo({
      success: function (res) {
        console.log(res.model)
        console.log(res.pixelRatio)
        console.log(res.windowWidth)
        console.log(res.windowHeight)
        console.log(res.language)
        console.log(res.version)
        console.log(res.platform)

        self.globalData.platform = res.platform;
      }
    })
    // wx.login({
    //   success: res => {
    //     // 发送 res.code 到后台换取 openId, sessionKey, unionId
    //   }
    // })
  },
  //微信登录
  wxlogin() {
    var that = this;
    wx.login({
      success(res) {
        if (res.code) {
          console.log(res);
          let params = {
            code: res.code,
          }
          getopenId(params).then(res => {
            let par = {
              openId: res.data.result.openid,
            }
            console.log(res.data.result)
            that.globalData.sesszionKey = res.data.result.sessionKey
            that.globalData.openId = res.data.result.openid
            // wx.setStorageSync('sesszionKey', res.data.result.sessionKey)
            login(par).then(res => {

              // if (res.data.status == 404) {
              //   wx.navigateTo({
              //     url: '/pages/register/register',
              //   })
              // } else {
                if (res.data.result.auth.token) {
                  let cookie = res.data.result.auth.token
                  console.log(cookie)
                  wx.setStorageSync("cookieInfo", cookie)
                  wx.reLaunch({
                    // url: '/pages/user/user',
                    url: '/pages/homepage/homepage',
                    // url: '/pages/bluetooth/bluetooth',
                  })
                } else {
                  wx.navigateTo({
                    url: '/pages/register/register',
                  })
                }
              // }
            })
          })

        } else {
          console.log("登录失败");
        }
      }
    })
  },
  globalData: {
    userInfo: null,
    appid: 'wx081f929323f7f767',
    secret: "56948926451dc1a2fdd99acc5b11cc95",
    sesszionKey: '',
    openId: '',
    btDevices: [{
      id: 1,
      rssi: -40,
      name: 'BT626',
      devId: '0',
      img: '/img/scan/2.png',
    }],
    platform: 'ios',
  }
})